package pdp;

public class Losango extends FiguraComposta implements FiguraGenerica{
	
	public void addLado(Reta r){
		if(this.retas.size() < 2){
			this.addParte(r);
		}else{
			System.out.println("Losango ja esta completo!");
		}
	}
	
	@Override
	public void draw() {
		System.out.println("Losango");
	}

	@Override
	public void area() {
		System.out.println("Area");
	}

	@Override
	public void volume() {
		System.out.println("Volume");
	}

}
