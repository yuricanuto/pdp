package pdp;

public class Quadrilatero extends FiguraComposta implements Figura{
	// Considerando Quadrilatero com lados diferentes
	public void addLado(Reta r){
		if(this.retas.size() < 4){
			this.retas.add(r);
		}else{
			System.out.println("Quadrilatero ja esta completo!");
		}
	}
	
	@Override
	public void perimetroFigura() {
		double ladoA, ladoB, ladoC, ladoD;
		double perimetro;
		
		if(this.retas.size() < 4){
			System.out.println("Quadrilatero incompleto!");
		}else{
			ladoA = this.retas.get(0).comprimento;
			ladoB = this.retas.get(1).comprimento;
			ladoC = this.retas.get(2).comprimento;
			ladoD = this.retas.get(3).comprimento;
			
			perimetro = ladoA + ladoB + ladoC + ladoD;
			System.out.println("Perimetro: "+perimetro);		
		}
	}

	@Override
	public void areaFigura() {
		double ladoA, ladoB, ladoC, ladoD;
		double area;

		if(this.retas.size() < 4){
			System.out.println("Quadrilatero incompleto!");
		}else{
			ladoA = this.retas.get(0).comprimento;
			ladoB = this.retas.get(1).comprimento;
			ladoC = this.retas.get(2).comprimento;
			ladoD = this.retas.get(3).comprimento;
			
			area = (ladoA*ladoB)/2.0 + (ladoC*ladoD)/2.0;

			System.out.println("Area: "+area);		
		}
	}

	@Override
	public void desenhaFigura() {
		
	}

}
