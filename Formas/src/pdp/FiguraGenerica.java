package pdp;

public interface FiguraGenerica {
	public void draw();
	public void area();
	public void volume();
}
