package pdp;

public class Circulo extends FiguraComposta implements Figura{
	// Considerando Lado com o raio e pegando valores depois
	public void addLado(Reta r){
		if(this.retas.size() < 1){
			this.retas.add(r);
		}else{
			System.out.println("Circulo ja esta completo!");
		}
	}

	@Override
	public void perimetroFigura() {
		double pi = 3.14;
		double raio;
		double perimetro;
		
		if(this.retas.size() < 1){
			System.out.println("Circulo incompleto!");
		}else{
			raio = this.retas.get(0).comprimento;
			
			perimetro = 2 * pi * raio;
			System.out.println("Perimetro: "+perimetro);			
		}

	}

	@Override
	public void areaFigura() {
		double pi = 3.14;
		double raio;
		double area;
		
		raio = this.retas.get(0).comprimento;		
		area = pi * Math.pow(raio, 2);
		System.out.println("Area: "+area);
	}

	@Override
	public void desenhaFigura() {
		
	}

}
