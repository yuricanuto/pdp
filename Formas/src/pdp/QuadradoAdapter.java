package pdp;

public class QuadradoAdapter extends Quadrado implements Figura{
	private Quadrado quadrado;
	
	public QuadradoAdapter(Quadrado quadrado) {
		super();
		this.quadrado = quadrado;
	}

	@Override
	public void perimetroFigura() {
		super.volume();
	}

	@Override
	public void areaFigura() {
		super.area();
	}

	@Override
	public void desenhaFigura() {
		super.draw();
	}
	
	
}
