package pdp;

public class LosangoAdapter extends Quadrado implements Figura{
	private Losango losango;
	
	public LosangoAdapter(Losango losango) {
		super();
		this.losango = losango;
	}
	
	@Override
	public void perimetroFigura() {
		super.volume();
	}

	@Override
	public void areaFigura() {
		super.area();
	}

	@Override
	public void desenhaFigura() {
		super.draw();
	}
}
