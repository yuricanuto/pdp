package pdp;

import java.util.ArrayList;
import java.util.List;

public class FiguraComposta implements Figura{
	List<Reta> retas = new ArrayList<Reta>();
	List<Ponto> pontos = new ArrayList<Ponto>();
	
	public void addParte(Reta r) {
		this.retas.add(r);
	}
	
	public void addParte(Ponto p) {
		this.pontos.add(p);
	}
	
	@Override
	public void perimetroFigura() {
		
	}

	@Override
	public void areaFigura() {
		
	}

	@Override
	public void desenhaFigura() {
		
	}

}
