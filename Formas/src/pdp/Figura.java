package pdp;

public interface Figura {
	public void perimetroFigura();
	public void areaFigura();
	public void desenhaFigura();
}
