package pdp;

public class Triangulo extends FiguraComposta implements Figura{
	
	public void addLado(Reta r){
		if(this.retas.size() < 3){
			this.addParte(r);
		}else{
			System.out.println("Triangulo ja esta completo!");
		}
	}
	
	@Override
	public void perimetroFigura() {
		double ladoA, ladoB, ladoC;
		double perimetro;
		
		if(this.retas.size() < 3){
			System.out.println("Triangulo incompleto!");
		}else{
			ladoA = this.retas.get(0).comprimento;
			ladoB = this.retas.get(1).comprimento;
			ladoC = this.retas.get(2).comprimento;
			
			perimetro = ladoA + ladoB + ladoC;
			System.out.println("Perimetro: "+perimetro);			
		}
	}

	@Override
	public void areaFigura() {
		double ladoA, ladoB, ladoC;
		double semiperimetro;
		double area;

		if(this.retas.size() < 3){
			System.out.println("Triangulo incompleto!");
		}else{
			ladoA = this.retas.get(0).comprimento;
			ladoB = this.retas.get(1).comprimento;
			ladoC = this.retas.get(2).comprimento;
			
			semiperimetro = (ladoA + ladoB + ladoC) / 2;
			area = Math.sqrt(semiperimetro*(semiperimetro-ladoA)*(semiperimetro-ladoB)*(semiperimetro-ladoC));
			System.out.println("Area: "+area);			
		}
	}

	@Override
	public void desenhaFigura() {
		
	}

}
