package estados;

public abstract class Estado {
	
	public Estado tick() {
		return null; }
	
	public Estado panic() {
		return null; }
	
	public Estado on() {
		return null; }
	
	public Estado off() {
		return null; }
	
	public abstract String status();
}
