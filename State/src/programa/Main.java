package programa;

import models.Semaforo;

public class Main {
	public static void main(String[] args) {
		Semaforo semaforo = new Semaforo();

		semaforo.on();
		semaforo.tick();
		semaforo.panic();
		semaforo.tick();
		semaforo.tick();
		semaforo.tick();
		semaforo.off();
		semaforo.on();

	}
}
