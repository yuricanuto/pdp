package models;

import estados.Amarelo;
import estados.Estado;

public class Semaforo {
	
	private Estado estado;
	
	public Semaforo() {
		this.estado = new Amarelo();
	}
 
	public void on() {
		this.estado = this.estado.on();
		System.out.println(estado.status());
	}
 
	public void off() {
		this.estado = this.estado.off();
		System.out.println(estado.status());
	}
 
	public void panic() {
		this.estado = this.estado.panic();
		System.out.println(estado.status());
	}
 
	public void tick() {
		this.estado = this.estado.tick();
		System.out.println(estado.status());
	}
 
	public String status() {
		return estado.status();
	}

}
