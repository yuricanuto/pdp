
public class BB extends Banco {

	public BB(Mediator m) {
		super(m);
	}

	@Override
	public void receberTransferencia(double valor) {
		System.out.println("Conta BB recebeu: " + valor);
	}

}
