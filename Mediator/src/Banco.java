
public abstract class Banco {
	protected Mediator mediator;
	
	public Banco(Mediator m){
		this.mediator = m;
	}
	
    public void fazerTransferencia(double valor) {
        mediator.enviar(valor, this);
    }
	
	public abstract void receberTransferencia(double valor);
}
