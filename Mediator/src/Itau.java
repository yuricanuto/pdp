
public class Itau extends Banco {

	public Itau(Mediator m) {
		super(m);
	}

	@Override
	public void receberTransferencia(double valor) {
		System.out.println("Conta Itau recebeu: " + valor);
	}
	
}
