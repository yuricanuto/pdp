public interface WeatherDataListener extends java.util.EventListener {
	
	void pegarTemperatura(WeatherDataEvent e);
	
	void pegarHumidade(WeatherDataEvent e);
	
	void pegarPressao(WeatherDataEvent e);
}
